from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

from profile.models import ProfileForm, Profile

@login_required
def edit(request):
    profile, _ = Profile.objects.get_or_create(user=request.user)
    if request.method == 'POST':
        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse(edit) + '?saved')
    else:
        form = ProfileForm(instance=profile)

    return render(request, 'profile/edit.html',
                  {'form': form,
                   'saved': (True if 'saved' in request.GET
                             else False)})
