# -*- coding: utf-8 -*-
from django.conf import settings
from django.db import models
from django.forms import ModelForm


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.CASCADE)

    nick = models.CharField('Ник', max_length=100, blank=True)
    url = models.URLField('URL', blank=True)
    city = models.CharField('Город', max_length=100, blank=True)

class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        fields = ('nick', 'url', 'city')
