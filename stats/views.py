from django.shortcuts import render

from django.contrib.auth.models import User

from comics.models import Comics
from livejournal.models import Post
from transcript.models import UnapprovedTranscription


def show(request):
    stat = {}
    stat['comics'] = Comics.objects.count()
    stat['unpublished'] = Comics.objects.filter(visible=False).count()
    stat['ready'] = Comics.objects.filter(visible=False, ready=True).count()
    stat['lj'] = Post.objects.count()
    stat['transcriptions'] = Comics.objects.exclude(transcription='').count()
    stat['unapproved_transcriptions'] = UnapprovedTranscription.objects.count()
    stat['authors'] = User.objects.count()
    stat['superusers'] = User.objects.filter(is_staff=True).count()
    return render(request, 'statistics/show.html', stat)
