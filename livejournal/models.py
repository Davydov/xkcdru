from django.db import models

from comics.models import Comics


class Post(models.Model):
    comics = models.OneToOneField(Comics,
                                  on_delete=models.CASCADE)

    url = models.URLField('URL')
    pid = models.IntegerField('Post id')
