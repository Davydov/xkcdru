from os.path import join

import django.contrib.auth.views
import django.contrib.sitemaps.views

from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.auth.views import LoginView, LogoutView

from comics.feeds import LatestComics
from comics.sitemap import ComicsSitemap

import comics.views
import transcript.views
import livejournal.views
import stats.views
import profile.views

admin.autodiscover()

sitemaps = {
    'comics': ComicsSitemap
}

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', comics.views.last,
        name='comics.last'),
    url(r'^num/$', comics.views.index_numbers,
        name='comics.index_numbers'),
    url(r'^img/$', comics.views.index_thumbnail,
        name='comics.index_thumbnail'),
    url(r'^(?P<comics_id>\d+)/$', comics.views.detail,
        name='comics.detail'),
    url(r'^(?P<comics_id>\d+)/(?P<timestamp>\d+)/$',
        comics.views.detail_unpublished,
        name='comics.detail_unpublished'),
    url(r'^random/(?P<comics_id>\d+)/$', comics.views.random,
        name='comics.random'),
    url(r'^random/$', comics.views.random),
    url(r'^random/untranscribed/(?P<comics_id>\d+)/$',
        transcript.views.random,
        name='transcript.random'),
    url(r'^random/untranscribed/$', transcript.views.random),
    url(r'^feeds/xkcd/$', LatestComics(), name='latest-comics'),
    # For users.
    url("login/",
        LoginView.as_view(template_name='login.html')),
    url("logoutt/",
        LogoutView.as_view(template_name='account/logout.html',
                           next_page='/'),
        name='logout'),
    url(r'^unpublished/$',
        comics.views.index_unpublished,
        name='comics.index_unpublished'),
    url(r'^edit/(?P<comics_id>\d+)/$',
        comics.views.edit, name='comics.edit'),
    url(r'^publish/(?P<comics_id>\d+)/$',
        comics.views.publish, name='comics.publish'),
    url(r'^review/(?P<comics_id>\d+)/$',
        comics.views.review, name='comics.review'),
    url(r'^add/$',
        comics.views.add, name='comics.add'),
    url(r'^profile/$',
        profile.views.edit, name='profile.edit'),
    url(r'^livejournal/(?P<comics_id>\d+)/$',
        livejournal.views.post, name='livejournal.post'),
    # Transcriptions.
    url(r'^transcript_form/(?P<comics_id>\d+)/$',
        transcript.views.show_form, name='transcript.show_form'),
    url(r'^transcript_save/(?P<comics_id>\d+)/$',
        transcript.views.add, name='transcript.add'),
    url(r'^transcript_thanks/(?P<comics_id>\d+)/$',
        transcript.views.thanks, name='transcript.thanks'),
    url(r'^transcript_edit/(?P<comics_id>\d+)/$',
        transcript.views.edit, name='transcript.edit'),
    url(r'^transcript_clear/(?P<comics_id>\d+)/$',
        transcript.views.clear_unapproved,
        name='transcript.clear_unapproved'),
    url(r'^unapproved/$',
        transcript.views.list_unapproved,
        name='transcript.list_unapproved'),
    url(r'^statistics/$',
        stats.views.show,
        name='statistics.show'),
    # Sitemaps.
    url(r'^sitemap.xml$', django.contrib.sitemaps.views.sitemap,
        {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'),
] + static('/i/',
           document_root=join(settings.MEDIA_ROOT, 'i/')
) + static('/t/',
           document_root=join(settings.MEDIA_ROOT, 't/')
)
